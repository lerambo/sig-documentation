# openKylin社区-documentation sig

openKylin社区-documentation，致力于完善openKylin社区文档，帮助用户更好更方便的使用openKylin系统，主要工作内容是书写各类说明文档，包括但是不限于教程、答疑等内容

# 基本信息

## 仓库地址

[sig-documentation](https://gitee.com/openkylin/sig-documentation.git)

手机用户因为码云的自适应问题默认看到的是README文件，仓库具体内容访问此链接：[手机端仓库目录](https://gitee.com/openkylin/sig-documentation/tree/master)

## 小组成员

- 陌生人
- chipo

## 通信方式

- 邮件列表：<documentation@lists.openkylin.top>，[邮件列表订阅页面](https://mailweb.openkylin.top/postorius/lists/documentation.lists.openkylin.top/)

# 工作内容

- 收集各种与openKylin有关的问题，并根据自己的理解写出对应的解决方法
- 书写各类教程，要求都可以在openKylin上使用，包括但是不限于教程、答疑等内容

# 贡献指南
## 内容要求
- 内容方向没有特殊要求，只要可以在openKylin上使用即可
- 内容需要照顾到新手用户，所有的流程都要尽可能详细
- 内容需要有标题、作者和创建时间，其它要求暂无
- 禁止带有侮辱性词汇，禁止政治敏感词汇
- 禁止发布广告
- 禁止发布违反法律法规的信息
- 文件名及分类文件夹尽可能言简意赅
- 结尾统一用.md的后缀，编码为统一的无BOM头的UTF-8
- 图片等资源统一放置在资源目录并分类
- 图片高度建议在640px左右、宽度不超过820px、图片一般为.png格式，大小不超过150K
- 为避免产权侵犯，引起纠纷，文档配图请使用原创图片或无版权图片
- 图片建议根据内容命名，只用数字序列不利于后续图片的继承
- 禁止任何分支强制推送
- 禁止推送到主分支，主分支不接受除dev分支以外的任何pr请求同时也不接受任何推送


## 参与方法

- 参与这项工作没有更多的要求，只要愿意尝试且按照指南开展工作，遇到疑问或无法决定的问题时同其他人讨论，就一定可以做出高质量的成果
- sig组成员在仓库上建立对应的分支，格式为dev-名字，例如dev-moshengren，dev-chipo，在自己的分支上书写对应的内容
- 非sig成员可以在gitee上fork一下仓库，在自己仓库的dev分支进行修改，然后按照[内容要求](#内容要求)添加、补充或修改内容，完成后通过提pr的方式来提交到主仓库,通过审核后即可合并
- 非sig成员可以通过上述邮件列表<documentation@lists.openkylin.top>或单独发送邮件给邮件列表所有者<documentation-owner@lists.openkylin.top>沟通来申请加入sig，建议先订阅，非订阅人员的邮件会被暂时拦截，邮件列表订阅连接：[documentation订阅页面](https://mailweb.openkylin.top/postorius/lists/documentation.lists.openkylin.top/)
- [sig申请加入流程](https://docs.openkylin.top/zh/SIG%E4%BD%BF%E7%94%A8%E6%89%8B%E5%86%8C/SIG%E7%BB%84%E7%9A%84%E7%94%B3%E8%AF%B7%E4%B8%8E%E6%92%A4%E9%94%80%E6%B5%81%E7%A8%8B)
- 参与贡献前可以先在仓库提一个Issue（只限于添加），说明想要书写的内容，方便查看是否有人正在写自己想写的内容，如果想要修改某些内容可以修改以后提pr
- 如果不太会写markdown格式的文档可以在搜索引擎搜索markdown文档语法或者通过邮件列表沟通，让sig成员帮忙转换

## 关于许可证

仓库默认使用CC BY-SA 4.0许可证，如果有其他需要可以在自己的文档下标明具体的CC许可证版本

以下内容插入文档末尾在文档平台生成页面的时候会自动变成对应的许可证
```
版权声明：本文为<xxx>原创，<xxx>修改，依据 [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 许可证进行授权，转载请附上出处链接及本声明。

```